#!/usr/bin/env python3

# Step 1
# Starting from this script (try it !)
# Extend the game by asking for a number
# between 1 and 100 until it has been guessed
# (uses a while loop, think about a break 
# statement or a flag variable "found" or
# "notfound")

# Step 2
# Create a function asknumber asking for
# user input until it is actually a decimal
# number
# - Hint: you can use strip() method on
#         strings to remove leading and
#         trailing spaces
# - Hint: you can use isnumeric() method
#         on strings to check if it can
#         be converted into an int (i.e.
#         it only contains digits)
# If the string cannot be converted into an int
# (i.e. isnumeric() is False) then ask the player
# again (while loop here also) until it's ok
# then the function returns the number
# Use that function instead of input()
# when asking for a number in the game.
# like this: guess = asknumber("...")

from random import randint

name = input("What is your name ? ")

print(f"Hello {name}!")

secret = randint(1, 100)

print("I picked a number between 1 and 100, can you guess it ? ")

# This will fail if the string cannot be converted into a int


while True: 
    guess = int(input("Your guess ? "))
    if guess < secret:
        print("Too small.")
    elif guess > secret:
        print("Too big.")
    else:
        print(f"Congratulations! That was {guess}!")
        break

