#!/usr/bin/env python3

class MyClassA:
    def __str__(self):
        return "__str__ here"

class MyClassB:
    def __repr__(self):
        return "__repr__ here"

class MyClassC:
    def __str__(self):
        return "__str__ here"
    def __repr__(self):
        return "__repr__ here"
