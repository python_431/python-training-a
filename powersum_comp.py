#!/usr/bin/env python3

# Rewrite this using the sum() function
# on a list and/or generator comprehension

def powersum(*args,n=1,to_float=False):
    res = 0
    for value in args:
        # same as :
        # res = res + value ** n
        res += value ** n
    if to_float:
        return float(res)
    else:
        return res

print(powersum(10,5,2))
print(powersum(10,5,2,to_float=True))
print(powersum(10,5,2))
print(powersum(10,5,42,12,5,n=3,to_float=True))
# No overflow with int, overflow with float
print(powersum(*range(1,11),n=345,to_float=False))

myValues = [1,2,3,4,5,7,42,-1]
print(powersum(*myValues,n=2))
