#!/usr/bin/env python3

fname = "AY38_G6_0.5_pH-3.50004_0001.ASC"

from pprint import pprint

with open(fname, encoding="latin1") as f:
    title = next(f).strip() # or f.readline().strip() 
    # Metadata
    # new in py 3.7 : 'walrus' operator (:=) it is assigning a value
    # and is an expression having that value
    # before you would have used while True: and break
    meta = {}
    while ( current := f.readline().strip() ) != '':
        name, value = current.split(':', 1)
        name = name.strip()
        value = value.strip()
        if '"' in value:
            value = value.replace('"', '')
            if value == '':
                continue # not considering empty values
        else:
            value = float(value)
        meta[name] = value
    pprint(meta)
    print("Processing Correlation")
    if f.readline().strip() == '"Correlation"':
        correlation = []
        while ( current := f.readline().strip() ) != '':
            row = [ float(v) for v in current.split() ]
            correlation.append(row)
        print('Correlation')
        pprint(correlation)
    # same idea for other sections...

