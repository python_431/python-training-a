#!/usr/bin/env python3

# Class definition

class Atom:
    def __init__(self, name, symbol, Z, A):
        self.name = name
        self.symbol = symbol
        self.Z = Z
        self.A = A
    ## TODO: create a "getter" (using @property)
    ## in order to return neutron_number as if it
    ## were a data attribute
    @property
    def neutron_number(self):
        return self.A - self.Z # right ?
    def __str__(self):
        return f"({self.A}){self.symbol}"
    def __repr__(self):
        return f"Atom({repr(self.name)}, {repr(self.symbol)}, {repr(self.Z)}, {repr(self.A)})"

# test code
if __name__ == '__main__':
    # A few instances
    He4 = Atom('Helium', 'He', 2, 4)
    He3 = Atom('Helium', 'He', 2, 3)
    
    H = Atom('Hydrogen', 'H', 1, 1)
    
    Ca12 = Atom('Carbon', 'Ca', 6, 12)
    Ca14 = Atom('Carbon', 'Ca', 6, 14)
    
    all  = [ He4, He3, H, Ca12, Ca14 ]
    
    # let's loop through all instances
    for atom in all:
        print(atom.name)
        print(atom) # here str is called
        print(f"  repr : ", repr(atom))
        print(f"  Mass number:    {atom.A}")
        print(f"  Atomic number:  {atom.Z}")
        print(f"  Neutron number: {atom.neutron_number}")
    
    H.neutron_number = 42
