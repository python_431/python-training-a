#!/usr/bin/env python3

# Rewrite this using the sum() function
# on a list and/or generator comprehension

def powersum(*args,n=1,to_float=False):
    # using a list comprehension would store
    # the collection of squares in memory:
    # res = sum( [ v**n for v in args ] )
    # using a generator comprehension is NOT:
    res = sum( v**n for v in args )
    res = float(res) if to_float else res
    return res

print(powersum(10,5,2))
print(powersum(10,5,2,to_float=True))
print(powersum(10,5,2))
print(powersum(10,5,42,12,5,n=3,to_float=True))
# No overflow with int, overflow with float
print(powersum(*range(1,11),n=345,to_float=False))

myValues = [1,2,3,4,5,7,42,-1]
print(powersum(*myValues,n=2))
